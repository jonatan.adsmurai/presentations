---
title: EuroSciPy 2019 - Tutorials' notes
theme: default
class:
  - invert
  - lead
size: 16:9
paginate: true
footer: EuroSciPy 2019 - Tutorials' notes
author: Jon

---

<!--
_paginate: false
_footer: 'Adsmurai - Machine Learning and Applied Research'
-->

# Euroscipy 2019
## Tutorials' notes

---

## Disclaimer: the following slides are more like personal notes converted to slides rather than an actual presentation.
 
---

<!--_class: lead-->
# 3D image processing with scikit-image

---

# 3D image processing with scikit-image

SciPy + Scikits [(list)](https://www.scipy.org/scikits.html)
_scikit-image_ for image processing
Insight Toolkit [(_itk_)](https://itk.org) as a tool for visualizing 3D images
_supplementary_code_ for working on 3D images

Links: [repo](https://github.com/alexdesiqueira/tutorials/tree/master/2019/euroscipy2019)

---

<!--_class: lead-->
# Deep Diving into GANs: From Theory to Production with TensorFlow 2.0

---

# Deep Diving into GANs: From Theory to Production with TensorFlow 2.0
#### Ideas
<!--
- You do not need to design a loss function if a discriminator designs one for you
-->
Use non-saturing loss function (avoids vanishing gradient)
Data → specialized architecture (default: FC layers)
Order is important: update D then train G
Stop when discriminator is completely fooled: random Real/Fake

#### TensorFlow 2.0
No graphs → track the gradient manually (GradienTape), ...

---
# Deep Diving into GANs: From Theory to Production with TensorFlow 2.0
#### _AshPy_
- Rapid model prototyping
- Enforcement of best practices & API consistency
- Remove duplicated and boilerplate code
- General usability by new project
- Based on Keras

Links: [repo](https://github.com/zurutech/ashpy)

---

# Deep Diving into GANs: From Theory to Production with TensorFlow 2.0
### Serving Models

Cold and warm invocations
AI Platform Prediction: production, pre-defined ML models for inference
Cloud function (GCP): experimentation, some coding but more flexible

Links: [repo](https://github.com/zurutech/gans-from-theory-to-production), [example1](https://blog.zuru.tech/gans-from-theory-to-production/), [example-interpolation](https://blog.zuru.tech/gans-from-theory-to-production/interpolation.html), [jupyter-notebook](https://hub.gke.mybinder.org/user/zurutech-gans-f-y-to-production-fv6ba8c7/notebooks/4.%20Production/4.1%20Serving%20Models%20using%20TF%202.0%20and%20Cloud%20Functions.ipynb)

---

<!--_class: lead-->
# Building data pipelines in Python: Airflow vs scripts soup

---

# Building data pipelines in Python: Airflow vs scripts soup
#### What we need to automate?
Easy tasks: frequent, clear success/failure, input handled by machines
More complex tasks: break into smaller and easier ones !

---

# Building data pipelines in Python: Airflow vs scripts soup
#### Consider
When?
Time limit?
Inputs?
Clear success or failure?
If fails what happens?
What provides or produces? In what way? To whom?
What happens after?

---

# Building data pipelines in Python: Airflow vs scripts soup
#### Pipelines
Reproducible, easy to productize, atomic, idempotency

#### Mottos
> Your Data is Dirty unless proven otherwise.

> All your Data is Important unless proven otherwise.


Links: [repo](https://github.com/trallard/opendata-airflow-tutorial.git)

---

<!--_class: lead-->
# Pandas 🐼

---

# Pandas 🐼
#### Tips

- Consider `__add__`  and `__radd__`
- `.loc` uses index whereas `.iloc` uses positions
- ⚠️ **Test** after tricky processing to ensure you have you want (shape, format, ...)
- ⚠️ Use `()` on multi-conditionals (operation precedence py != pandas)
- 🛑 Stop using `in_place`, it's going to disappear asap.

---

# Pandas 🐼
#### Tips

- **Try** to avoid coding loops
- Check operations: pivot, stack, unstack, ...
- 🛑 Stop using CSV: inefficient, loses info on I/O, cannot handle dates, ...
- Use [HDF](https://en.wikipedia.org/wiki/Hierarchical_Data_Format), [parquet](https://parquet.apache.org) or pickle [(benchmark)](https://towardsdatascience.com/the-best-format-to-save-pandas-data-414dca023e0d)
- If the output is data: notebooks 👌
- If the output is software: method chaining 👌

---

# Pandas 🐼
there are tutorials for homework!

---

<!--_class: lead-->
# Sufficiently Advanced Testing with Hypothesis

---

# Sufficiently Advanced Testing with Hypothesis
#### Ideas

Assertion: always true unless there is a bug

Use uncovered lines not its percentage

Compare your implementation with trusted implementation or check its properties

Lots of extensions: pandas, numpy, ...

---

# Sufficiently Advanced Testing with Hypothesis
#### Ideas

Extend strategies using `map` and `filter` (`map` > `filter`)

Extend by `build`:

```python
def f(a: int)
  return str(a)
build(f)
```

Custom strategies: `@composite`

---

# Sufficiently Advanced Testing with Hypothesis
#### Tactics: what do we test?

- Auto-manual testing: `assert expected == output`
- Oracle tests (even if partial): `assert sorted(x) == mysort(x)`
- Partial specification: $min(x) \le mean(x) \le max(x)$
- Metamorphic testing: $\Delta_1 input \implies \Delta_2 output$
- Hyper-properties
  - Invariants `assert count(list) == count(sorted(list))`
  - Round-trips: $x = g(f(x)), ~g = f^{-1}$ `assert lower(upper(x)) == x`


---

# Sufficiently Advanced Testing with Hypothesis
#### Tactics

_Reverse oracles_: generate an answer, ask the oracle for a matching question, test that code gets the answer [(external example)](https://www.hillelwayne.com/post/hypothesis-oracles/)
Test that it does not crash (no assert): embarrassingly effective!

Links: [repo (maintainer)](https://github.com/Zac-HD/escape-from-automanual-testing)

---


# Sufficiently Advanced Testing with Hypothesis
#### Session 2 is different!

---

<!--_class: lead-->
# Introduction to SciPy

---

# Introduction to SciPy
#### Notes

Allows you to do serious science
[phyphox](phyphox.org): do measurements with your mobile phone

Links: [repo](https://github.com/gertingold/euroscipy-scipy-tutorial)

---

<!--_class: lead-->
# Introduction to scikit-learn: from model fitting to model interpretation

---

# Introduction to scikit-learn: from model fitting to model interpretation
#### Notes

Statistics → ML → DL 
Homogeneous API over heterogeneous algorithms

#### Check
[seaborn](https://seaborn.pydata.org): statistical data visualization
[`pandas_profiling`](https://github.com/pandas-profiling/pandas-profiling): extends `df.describe()` by `df.profile_report()`
[`sklearn.pipeline.make_pipeline`](https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.make_pipeline.html): easier than coding train/validation

---

# Introduction to scikit-learn: from model fitting to model interpretation
#### Tips

`%matplotlib notebook` (jupter-notebook)
`%matplotlib inline` (jupyter-lab)

`>2k` data samples for ML
Use DummyClassifer to build a baseline or to check for problems
Use KFold to determine how stable is our classifier

Links: [repo](https://github.com/lesteve/euroscipy-2019-scikit-learn-tutorial), [slides](http://ogrisel.github.io/decks/2017_intro_sklearn)

---

# Introduction to scikit-learn: from model fitting to model interpretation

1. Split features between numerical and categorical
1. Encode each type of feature accordingly
1. HistGradientBoostingClassifier (order and 1-enc does no impact on it)

###### Numerical
You may apply scaling to numerical ones: depending on the model and features
⚠️  It may be detrimental
###### Categorical 
If there is no implicit order on a feature: one hot enconding (a need for linear models)

---

# Introduction to scikit-learn: from model fitting to model interpretation
#### Hyper-parameters tuning

GridSearch, RandomSearch, LogisticRegressionCV
Check whether *CV exists as they are more optimized than RandomSearch.

