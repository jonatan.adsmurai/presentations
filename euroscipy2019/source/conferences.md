---
title: EuroSciPy 2019 - Conference's notes
theme: default
class:
  - invert
  - lead
size: 16:9
paginate: true
footer: EuroSciPy 2019 - Conference's notes
author: Jon

---

<!--
_paginate: false
_footer: 'Adsmurai - Machine Learning and Applied Research'
-->

# Euroscipy 2019
## Conference's notes

---

## Disclaimer: the following slides are more like personal notes converted to slides rather than an actu$

---

<!--_class: lead-->
# Keynote: From Galaxies to Brains! - Image processing with Python

---

# Keynote: From Galaxies to Brains! - Image processing with Python
Problems measuring dark matter and galaxies: sparsity, blurring, ...
Use image processing a lot
Start using CNNs (detect galaxies blending), GANs (calibration), ...
Python Sparse data Analysis Package [(pysap)](https://github.com/CEA-COSMIC/pysap)

---

<!--_class: lead-->
# Sufficiently Advanced Testing with Hypothesis II

---

# Sufficiently Advanced Testing with Hypothesis II
#### Tips: design testeable code

- Inmutable data
- Canonical formats
- Well-defined interfaces
- Separate I/O and logic
- Explicit args for all deps
- Deterministic behaviour
- Lots of assertions

---

# Sufficiently Advanced Testing with Hypothesis II
#### Tips for easy testeable code

> arrange, act, assert
> given, when, then

Cars should not condifently choose a different acction if the camera feed has
- slight changes in contrast or brighness
- a small skew or offset or blur added
- light rain or fog added

[errors in CV](https://deeplearningtest.github.io/deepTest/)

---

# Sufficiently Advanced Testing with Hypothesis II
#### Tips

Oracle testing: good for refactoring or optimization `assert fast_f(x) == f(x)`
Testing on NNs
Stateful testing (NN, DDBB, ..)
Use `note()` for logging
Configurable from `hypothesis.settings`

---

# Sufficiently Advanced Testing with Hypothesis II
#### When do not use Hypothesis ?

..extract from slides.. (TODO: wait for newer versions to include it)

---

<!--_class: lead-->
# What about tests in Machine Learning projects?

---

# What about tests in Machine Learning projects?

Why? ML algorithms fail silently
Bugs in data and in code
A good test is: readable, simple, thorough, specific, **repeatable (stable)**, isolated, **fast**
TDD for DS ? (TODO: check [web](http://sdg.jlbl.net/slides/tests_for_datascientist/presentation.html)

#### Four phases in a project
Data loading, preprocess, ML algorithm, API interface

---

# What about tests in Machine Learning projects?
###### **Data loading**, preprocess, ML algorithm, API interface

Mock to isolate and then unit test

###### Data loading, **preprocess**, ML algorithm, API interface

It's naturally functional programming: unit test
Think about of invariants (_hypothesis_): $norm(x) \in [0,1]$
Add a few trivial test with simple examples

---

# What about tests in Machine Learning projects?
###### Data loading, preprocess, **ML algorithm**, API interface

Hard to unit test: iterative, stochastic, suffer from oracle problem, slow

Overcome iterativity:
- Unit-test what you can: decreasing loss, ..
- Component-test: test larger chunks of code

---

# What about tests in Machine Learning projects?
###### Data loading, preprocess, **ML algorithm**, API interface

Overcome stochasticity: fix random seeds, average multiple runs

Overcome oracle problem:
- Differencial testing: against other implementations
- Metamorphic testing: non-regression between old and new implementation
- Reference tests (low coverage & hard to maintain): against a random or uniform baseline, **tdda**

Overcome slow speed: 2 test suites, 🦔💨 + 🐢 (nightly run)

---

# What about tests in Machine Learning projects?
Extracted from Test-Driven Data Analysis (TDDA) 1-page summary [(docs)](https://tdda.readthedocs.io/en/v1.0.30/overview.html)
![w:720](images/tdda_image.png)

---

# What about tests in Machine Learning projects?
###### Data loading, preprocess, ML algorithm, **API interface**
Have "just one" end-to-end test.

#### Tips
> Anyting is better than nothing

Have automated tests
Have a CI stage
Cheer up! testing ML is hard but not hopeless


Links: [repo](https://github.com/SdgJlbl/slides/tree/master/tests_for_datascientist)
---

# Open-source projects update

...
[casualimpact](https://github.com/dafiti/causalimpact)
[dask](https://dask.org)
...

---

<!--_class: lead-->
# Constrained Data Synthesis

---

# Constrained Data Synthesis

Skeptical about using synth data in ML. Who is first? 🥚 or 🐔
<!-- ML wants to discover patters, if we can generate actual data is that we already have this
patterns, otherwise, we are feeding the algorithm with non-real patterns. -->
Test-driven data analysis (TDDA) [repo](https://github.com/tdda), [web](http://www.tdda.info/pdf/), [one-pager](http://stochasticsolutions.com/pdf/TDDA-One-Pager.pdf)
String generation from regular expressions: **xerby** 🚧
Gets constrains from data: type, range, regex, ... automatically

Links: [slides](http://www.tdda.info/pdf/constrained-data-synthesis-euroscipy-2019.pdf)

---
<!--_class: lead-->
# Dashboarding with Jupyter notebooks, voila and widgets

---

# Dashboarding with Jupyter notebooks, voila and widgets

[*ipywidgets*](https://ipywidgets.readthedocs.io/en/latest/index.html): interactive jupyter notebooks (sliders, ..)
[*voila*](https://github.com/QuantStack/voila): hides implementation details, shows output or interfaces, extensible by JS
[*Ipyvuetify*](https://github.com/mariobuikhuizen/ipyvuetify): beautify widgets with VUE
[Deploying](https://voila.readthedocs.io/en/latest/deploy.html): heroku, ngrok, binder

---

<!--_class: lead-->
# Modin: Scaling the Capabilities of the Data Scientist, not the machine

---

# Modin: Scaling the Capabilities of the Data Scientist, not the machine
#### Optimize how users interactuate

Best practices:
- Limit your API (pandas +280 methods for DF, Series,, ..)
- Lazy evaluation (pandas do no use it ☹)

Links: [repo](https://github.com/modin-project/modin), [pyarrow](https://arrow.apache.org/docs/python/)

---

<!--_class: lead-->
# Controlling a confounding effect in predictive analysis

---

# Controlling a confounding effect in predictive analysis

Classical approach: general linear model
Propose: specific test set where cofound prediction vars are independent

- Based on anti-mutual information sampling
- Its non-parametric


Links: [repo](https://github.com/darya-chyzhyk/confound_prediction), `pip install confound-prediction`

---

# Controlling a confounding effect in predictive analysis
#
#
#
#
#
#
#
#
#
#
![bg w:400 invert:100%](https://github.com/darya-chyzhyk/confound_isolating_cv/raw/master/docs/Cross_validation_classic.svg?sanitize=true)
![bg w:400 invert:100%](https://github.com/darya-chyzhyk/confound_isolating_cv/raw/master/docs/Cross_validation_confound_isolation.svg?sanitize=true)


---

<!--_class: lead-->
# The Rapid Analytics and Model Prototyping (RAMP) framework: tools for collaborative data science challenges

---

# The Rapid Analytics and Model Prototyping (RAMP) framework

Reproducibility!
Links: domain-specific people with data scientists
Apps: for training people, compare solutions, ...

Compared to Kaggle:
- It is open source
- It allows constrained pipeline

---

# The Rapid Analytics and Model Prototyping (RAMP) framework

As an organizer, you:
- Prepare data
- Define a ML pipeline
- Write an starting-kit

As a participant, you:
- Develop a block of the ML pipeline
- Submit
- Iterate


Links: [ramp studio](https://ramp.studio), [1](https;//paris-saclay-cds.github.io/ramp-docs)

---

<!--_class: lead-->
# Visual Diagnostics at Scale

---

# Visual Diagnostics at Scale
Steering ML model selection and diagnostics: [scikit-yellowbrick](https://www.scikit-yb.org/en/latest/), [repo](https://github.com/rebeccabilbro)
Scaling problems are dataset-specific: ⇈features, ⇈samples, ..

_Heteroskedasticity_: refers to the circumstance in which the variability of a variable is unequal across the range of values of a second variable that predicts it.

<!--
In a regression problem: predicted <-> residuals 2dplot: we want random plots, if there is
structure our model is missing sth

Unit-test images? Problematic: depends on renders, is slow..
-->

`VisualPipeline()`for building a pipe of plots

> Models are agregations of data, so are visualization.

> Use visualization to steer your models.

---

<!--_class: lead-->
# Histogram-based Gradient Boosting in scikit-learn

---

# Histogram-based Gradient Boosting in scikit-learn
> In ML individual errors are not a big deal, we want to be good at average

[pygbm](https://github.com/ogrisel/pygbm) implementation is better than scikit-learn's one (👷🚧 moving into scikit)

This is as fast as C++:

```py
from numba import njit, prange

@njit(parallel=True)
def spam():
  for ham in prange():
    ...
  return
```

---

# Histogram-based Gradient Boosting in scikit-learn

Non-gaussian histogram of labels to reduce error: $(Exp \circ FitLin \circ Log)(labels)$

Random forest: 👌 but in ⇈memory, ~🐢 for predictions

Histogram-based GBM are: faster, better, x100 smaller, early stopping, ...

Partial dependence plots for tree ensembles [(docs)](https://scikit-learn.org/stable/modules/generated/sklearn.inspection.partial_dependence.html#sklearn-inspection-partial-dependence)

---

<!--_class: lead-->
# The Magic of Neural Embeddings with TensorFlow2

---

# The Magic of Neural Embeddings with TensorFlow2

We may be not interested in the "output" but on the embedding

Use the embedding to reduce dimensionality

Stabilize the emdedding to stabilize its visualization, so it's easy to track differenes between changes: transfer learn (small lr) or force (train from scratch and use the embedding as a loss)

Links: [DJCordhose](https://github.com/DJCordhose), [slides](bit.ly/euroscipy-embeddings)

---

<!--_class: lead-->
# High quality video experience using deep neural networks

---

# High quality video experience using deep neural networks

Reconstruct a high quality image from a compressed low quality image
- Deep Residual Net for reconstruction
- Trained on 128x128 MS-COCO
- ICCV'17, TMM'19

Links: [web & paper](https://www.micc.unifi.it/projects/deep-compression-artifact-removal/), [repo](https://github.com/peteanderson80/bottom-up-attention)

---

# High quality video experience using deep neural networks

Architecture:
```txt
                   |‾‾‾‾‾‾‾‾‾‾‾|
             LQ --‣| Generator | --‣ |‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾|
                   |___________|     | Discriminator | --> LQ/HQ ?
             HQ -------------------‣ |_______________|
```

Error of the loss function: depens on if a human or a computer is 'visualizing' an image (surveillance)

---

# High quality video experience using deep neural networks

Metrics for natural images:
- The Natural Image Quality Evaluator (NIQE)
- Blind/Referenceless Image Spatial Quality Evaluator (BRISQUE)

Results:
- SSIM is better than MSE but results are smooth
- GAN recovers high spatial frequency

---

# High quality video experience using deep neural networks

Some irrevelant errors confuses system
Adding super resolution in front of a generator allows to further reduce required bandwidth x10

Netflix's metric: [VMAF](https://github.com/Netflix/vmaf)

Video conferences: GAN for faces (keep quality reducing x150 BW)
- Inverted residual blocks

---

# High quality video experience using deep neural networks

Takeaways:
- GANs are great for image enhancement
- Do not trust std signal basd metrics
- Humans --> semantic tasks --> no reference --> full referencs

[L.Galteri, L Seidenari, et al. papers](http://www.micc.unifi.it/seidenari/publications/)

---

<!--_class: lead-->
# Keynote: In the shadow of the black hole

---

# Keynote: In the shadow of the black hole

Very-long-baseline interferometry [VLBI](https://en.wikipedia.org/wiki/Very-long-baseline_interferometry): multiple observations to render the same image
Continuous High-resolution Image Reconstruction using Patch priors, CHIRP [paper](http://people.csail.mit.edu/klbouman/pw/papers_and_presentations/cvpr2016_bouman.pdf)
> CHIRP [...] to turn the entire planet into a large radio telescope dish.

Correlation between sources --> calibration to amplify the signal

Links: [Dynamic Nested Sampling](https://github.com/joshspeagle/dynesty), [Event Horizon Telescope repo](https://github.com/eventhorizontelescope), [Image reconstruction challenge](http://vlbiimaging.csail.mit.edu/imagingchallenge)

---

<!--_class: lead-->
# A practical guide towards algorithmic bias and explainability in machine learning

---

# A practical guide towards algorithmic bias and explainability in machine learning

> Any non-trivial decision holds a bias.

> It's impossible to "just remove bias" as the purpose of ML is to discriminate towards right answer.

- Statistical bias vs A-priori bias
- Explainability is key
- Explainability is not Interpretability

Tradeoff: reduces accuracy, needs more time on feature engineering, ...

Align train ds to your expectation on your test ds (train ds does not need to be
full balanced)

---

# A practical guide towards algorithmic bias and explainability in machine learning

An eXplainability toolbox for ML, [XAI](https://github.com/EthicalML/xai)
Monitoring and explaining ML models, [alibi](https://github.com/SeldonIO/alibi)
ML deployment to Kubernetes, [seldon](https://github.com/SeldonIO/seldon-core)

Links: [repo](https://github.com/EthicalML/explainability-and-bias), [presentation](https://ethicalml.github.io/explainability-and-bias)

Others: Interpretable ML: A Guide for Making Black Box Models Explainable, [web](https://christophm.github.io/interpretable-ml-book/)

---

<!--_class: lead-->
# PyPy meets SciPy

---

# PyPy meets SciPy

Specific libs are ok but code has to be adapted to their API. Writing in C is painful

> Pypy makes pure-Python fast (30min to 3sec 1M adds)

Abstractions for free, makes Python 1st-class language for HPC

**Fast** on running the same code over and over
**Slow** on short scripts and tests

Issues: need to underst GC and JIT to use all potential, small community
[HPy](https://github.com/pyhandle/hpy): a better API for Python

---

<!--_class: lead-->
# Deep Learning for Understanding Human Multi-modal Behavior

---

# Deep Learning for Understanding Human Multi-modal Behavior

###### NOTE: I was late on this 15min-presentation. Need to read the paper to understand what it is truly about.

**Co-learning**: aiding the modeling of a (resource poor) modality by exploiting knowledge from another (resource rich) modality

Parallelism in co-learning: parallel, non-parallel, hybrid

Links: [arxiv](https://arxiv.org/abs/1705.09406), [github](https://github.com/ricoms)

---

# Deep Learning for Understanding Human Multi-modal Behavior

![width:680](images/parallelism_in_colearning.png)

- Parallel data: co-trainig, transfer learning
- Non-paralel data: zeroshot learning, concept grounding, transfer learning
- Hybrid data

<!--
pa: modalities are from the same dataset and there is a direct correspondence between instances
np: modalities are from different datasets and do not have overlapping instances, but overlap in general categories or concepts
hy: the instances or concepts are bridged by a third modality or a dataset
-->
