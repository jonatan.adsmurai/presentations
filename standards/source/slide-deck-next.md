---
title: MLAAR new standard proposals
theme: gaia
class:
  - invert
  - lead
size: 16:9
paginate: true
footer: MLAAR - Standard proposals 1
---

<!-- 
_paginate: false
_footer: ''
-->
# MLAAR 
## Standard proposals 1

---

#
#
#
#
#

We are ok. There's no pains to get rid of. But, 
here are some tools that could **help us** or **improve our code**.

#
#
#
#
#

# DISCLAIMER: <!-- fit --> some of them are not mean to be *standards* but to let you know they exist.

---

### + pytest plugins
### + code formatter

---

# pytest plugins

- Change (slightly) how to code
  - pytest-mock
  - pytest-freezegun

- Do not change how to code
  - pytest-cov
  - pytest-sugar
  - pytest-eradicate

---

# (other) pytest plugins

- Just to know they exist
  - pytest-watch
  - pytest-testmon
  - pytest-xdist

---

# pytest-mock
#### do better mocking

---


# pytest-mock
#### what we do (mock)

```python
import mock
@mock.patch('os.remove')
@mock.patch('os.listdir')
@mock.patch('shutil.copy')
def test_unix_fs(mocked_copy, mocked_listdir, mocked_remove):
    ...
```

---

# pytest-mock
#### what we do (mock)

```python
def test_unix_fs():
    with mock.patch('os.remove'):
        ...
        with mock.patch('os.listdir'):
            ...
    with mock.patch('shutil.copy'):
        ...
```

---

# pytest-mock
#### what we could do

```python
# No import needed
def test_...(mocker, ...):
    mocker.patch('os.remove')       
    mocker.patch('os.listdir', return=[])       
    mocker.patch('shutil.copy', side_effect=Exception)       
```

Pros: fixtures and parametrize, autoclose

[pytest-mock](https://pypi.org/project/pytest-mock/)

---

# pytest-cov
#### code coverage

```bash
INTERNALERROR> PermissionError: [Errno 13] Permission denied: '/app/.coverage'
```

---

# pytest-freezegun
#### let your Python tests travel through time

---

# pytest-freezegun

```python
@freeze_time("2012-01-14")
def test():
    assert datetime.datetime.now() == datetime.datetime(2012, 1, 14)

@freeze_time("Jan 14th, 2020", tick=True)
def test_nice_datetime():
    assert datetime.datetime.now() > datetime.datetime(2020, 1, 14)

def test_not_using_marker(freezer):
    now = datetime.now()
    time.sleep(1)
    later = datetime.now()
    assert now == later
```

---

# pytest-sugar
#### shows failures and errors instantly + progress bar

---

# pytest-sugar

![demo](https://pivotfinland.com/pytest-sugar/img/video.gif)

---

# pytest-eradicate
#### detects commented out code

---

# pytest-eradicate

```python
#import os
# from foo import junk
#a = 3
a = 4
#foo(1, 2, 3)

def foo(x, y, z):
    # print('hello')
    print(x, y, z)

    # This is a real comment.
    #return True
    return False

```

---

# pytest-eradicate

```python
a = 4

def foo(x, y, z):
    print(x, y, z)

    # This is a real comment.
    return False
```

---

# pytest-xdist
#### distributed tests 

---

# pytest-xdist

On local just add, 
```bash
py.test ... -n auto
```

On remote,

```
python socketserver.py
pytest -d --tx socket=192.168.1.102:8888 --rsyncdir mypkg mypkg
```
  
It does not split tests which are parametrized or use hypothesis.

---

# pytest-testmon
#### selects and re-executes only tests affected by recent changes

# pytest-watch
#### continuous pytest runner

---

# code formatters

- yapf
- black

---

# yapf
#### lots of options but recommends not to overcustomize
styles: pep8, chromium, facebook, google

# yapf

- super customizable: styles, params, penalty points ...
- no idempotent
- does not format py3.6+ features
- quadratic

---

# black 

- nearly no options so you do not have to discuss
  - *linewidth* and *do-not-check-single-quoted-strings*
- idempotent
- faster

---

# Bonus track

---

# Bonus track
#### What about *docstring-ing* the fixtures?
```sh
pytest --fixtures ; pytest --fixtures-per-test
```
![](no-docstring.png)
![](docstring.png)

<!--
```txt
----- fixtures used by test_default_error_handler ----
(predict_api/test/unit/flaskr/views/test_predict.py:83) 
------------------------------------------------------
correct_request_body
    no docstring available
test_client
    no docstring available
```
---

# Bonus track
#### What about docstring-ing the fixtures?

```txt
----- fixtures defined from pytest_docker -----
docker_compose_file [session scope]
    Get the docker-compose.yml absolute path.
    Override this fixture in your tests if you need a custom location.

docker_compose_project_name [session scope]
    Generate a project name using the current process' PID.
    Override this fixture in your tests if you need a particular project name.
``` 
-->

---

# Links

pytest-plugins proposals 
[mock][mock], [cov][cov], [eradicate][eradicate], [sugar][sugar], [freezegun][freezegun]

other pytest-plugins
[testmon][testmon], [watch][watch], [xdist][xdist]

code formatters proposals
[yapf][yapf], [black][black]


[mock]: https://github.com/pytest-dev/pytest-mock
[cov]: https://github.com/pytest-dev/pytest-cov
[eradicate]: https://github.com/spil-johan/pytest-eradicate
[sugar]: https://github.com/Frozenball/pytest-sugar
[freezegun]: https://github.com/ktosiek/pytest-freezegun
[testmon]: https://github.com/tarpas/pytest-testmon
[watch]: https://github.com/joeyespo/pytest-watch
[xdist]: https://github.com/pytest-dev/pytest-xdist
[yapf]: https://github.com/google/yapf
[black]: https://github.com/psf/black

---

<!--
paginate: false
_footer: ' '
-->
# <!-- fit --> Hope you enjoyed it ! :grin:
![bg opacity 70% right](badge.svg)
![w:60px](adsmurai.png) Adsmurai

Passeig de Gràcia, 60 4º
08007 Barcelona
Phone: +34 931 222 301
Spain

📧 research@adsmurai.com
