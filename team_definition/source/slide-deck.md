<!-- Directives
title: MLAAR definition activity
description: created using Marp [deprecated] https://github.com/yhatt/marp
$theme: gaia
template: invert
$size: 16:9
page_number: false
footer: Adsmurai - Machine Learning and Applied Research - Team definition
-->

<!-- *footer: -->
# Ice breaker :icecream:
##### Choose a character you like from a film, book, cartoons, ... and tell us something
![bg opacity](dragonball.jpg)

---

# *Machine Learning and Applied Research* - Team definition
<!-- *footer: -->
# ![center 40%](badge.svg)

---

## Are you having an Identity crisis? :scream:
##### Who am I ?
# ![](indian-heroes.jpg)

---

## Are you having an Identity crisis? :scream:
##### Here I am !
##### ![](mlaar.jpg)

---

## âď¸ Why is it happening to me ? :weary:

- Low self-esteem
	- Do you like yourself ?
	- Are you good at anything useful ?
- Low self-efficacy
    - Do you believe you have control over the important aspects of your life?
    - Are you "stuck" in a situation you don't like but can't leave? Why do you think that is the case?
	- Are you a weak person? In what way? Why is that?

<!-- NOTE: Read but do not answer -->

---

# What can I do ? :sob:
##### Should I ~~do~~ take drugs :pill: ?
<!-- NOTE: No!.... Well yes! -->

---

#  There's two ways :sunglasses:
<!-- # ![](matrix-pills.jpg) -->

![bg](matrix-pills.jpg)
You take the blue pill - the story ends, you wake up in your bed and believe whatever you want to believe.

You take the red pill - you stay in Wonderland, and I show you how deep the rabbit hole goes

### What will you choose ?

---

# :red_circle: Let's go full fantasy !
##### ![70%](gandalf.jpg)

---

# Define the team by
# creating  RPG characters
# ![70%](comic.jpg)
<!-- NOTE: -->

---

# 3 Steps
### Aspects, Prose, Interpretation

---

# 4 aspects
### High concept, Trouble, Personal story, Crossroad
<!--
1. High concept
2. Trouble
3. Personal story
4. Crossroad
-->

---

# High concept
### One sentece that defines the PC

<!--
- Sums up who they are
- Tells what they're doing for the living
- Tells what makes them different from others

Think on
- In what they could help you
- How could complicate your life
-->

---

# Trouble

### How you use to complicate your life ?

<!--
Ideas
- Personal weakness
- Recurrent enemy
- Important obligation
- ...
-->
---

# Personal story

### How they reacted to an important situation they were involved ?

<!-- Problem âĄď¸ Resolution -->

---

# Crossroad

### How they reacted to an important situation they were passing by ?


<!-- One person starts a story leaving the resolution open, and other person explains how they PC resolves it. -->

---

# Example

1. Concepto: arquitecto romano *polĂ­glota*
2. ComplicaciĂłn: *amante de la naturaleza*
3. Historia inicial: se iba a casar y lo dejaron plantado por alguien mĂĄs rico, ahora es *precavido* y pasa del dinero
4. Cruce de caminos: se encuentra un conflicto en el mercado, un tendero acusa a un pobre de robarle una manzana y este dice que se la han dado. Que hace? Le paga la manzana al tendero, *humanitario*

---

# *Prose* it
#### Using a (some) sentences using *aspects*

---

# Example

*polĂ­glota*, *amante de la naturaleza*, *precavido*, *humanitario*

Es capaz de hablar con todos,
cuidadoso al proyectar,
si no va a dejar mejor el paisaje no lo hace,
su objetivo final no es el dinero,
evita los aprovechados,
asume algunos pequeĂąos costes si esto mejora el mundo.

---

# :large_blue_circle: Return back to reality
#### Re-interpret the definition as it was defining the team

---

# Example
- EstĂĄ cerca del usuario, habla su idioma y entiende sus problemas.
- SĂłlo trabaja donde hay una oportunidad clara de mejora.
- AportaciĂłn a la ciencia, no sĂłlo porque aporte dinero.
- No le hace el trabajo a los demĂĄs, sino que los ayuda, especialmente si con poco esfuerzo ayuda mucho.

---

# Build a party
#### Use items to define the team
#### (aka Join âĄď¸ Discuss âĄď¸ Select)

---

<!--
page_number: false
footer: Adsmurai - Machine Learning and Applied Research - Team definition activity Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â by Jon
-->
# Hope you enjoyed it ! :grin:
<!-- ![center 1%](badge.svg) -->

<style>
* {
  box-sizing: border-box;
}
.column {
  float: left;
  width: 50%;
  padding: 60px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
.line{
	margin-bottom: -0.8cm;
}
.mid{
	padding-top: 1.7cm;
}
</style>
<div class="row">
  <div class="column" align="right">
    <h2>
      <img src="adsmurai.png" height="80" style="float:left; margin-left:100px; margin-top:-8px" align="">
      Adsmurai
    </h2>
    <p class="line">
	Passeig de GrĂ cia, 60 4Âş
    <p class="line">
	08007 Barcelona
    <p class="line">
	Phone: +34 931 222 301
    <p class="line">
	Spain
  </div>
  <div class="column">
    <p class="mid">
    đ§ research@adsmurai.com
  </div>
</div>
