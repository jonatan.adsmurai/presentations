---
title: MLAAR definition activity
theme: gaia
class:
  - invert
  - lead
size: 16:9
paginate: false
footer: 'Team definition'
description: created using Marp Next https://github.com/marp-team/marp
instruction: docker run --rm --init -v $PWD:/home/marp/app -e LANG=$LANG -p 8080:8080 -p 52000:52000 marpteam/marp-cli -s --allow-local-files .
author: Jonatan
---

<!-- _header: '' _footer: ' ' -->
# Ice breaker :icecream:
##### Choose a character you like from a film, book, cartoons, ... and tell us something
![bg opacity:.2](dragonball.jpg)

---

<!-- NOTE: Why are we here ? -->
# <!-- fit --> *Machine Learning and Applied Research*
# Team definition
<!-- _header: '' _footer: ' ' -->
# ![w:360](badge.svg)


---

<!-- NOTE: We don't know who we are -->
## Are you having an Identity crisis? :scream:
##### Who am I ?
# ![h:480](indonesian-heroes.jpg)

---

<!-- NOTE: We are going to find ourselves to define the team -->
## Are you having an Identity crisis? :scream:
##### Here I am !
##### ![h:480](mlaar.jpg)
---

## Mlaar definition

<style scoped>
li{
  font-size:70%
}
</style>

- **Real Name**
  * Mlaar
- **Identity/Class**
  * Extra-terrestrial mutate
- **Occupation**
  * King of Covox
- **Known Relatives**
  * Unidentified father (deceased), unidentified uncle
- **Powers/Abilities**
  * Able to elongate his body like elastic
- **Enemies**
  * Taring Emas (Golden Fang)
- **History**
  * After his father was deposed, Prince Mlaar was rescued by his scientist uncle, who gifted him with the ability to bend and stretch like elastic. When the hero Gundala visited his world, he assisted Mlaar in winning back his throne.

---

<!-- NOTE: Medical symptoms -->
<!-- NOTE: Read but do not answer -->
## ⚕️ Why is it happening to me ? :weary:
- Low self-esteem
	- Do you like yourself ?
	- Are you good at anything useful ?
- Low self-efficacy
    - Do you believe you have control over the important aspects of your life?
    - Are you "stuck" in a situation you don't like but can't leave? Why do you think that is the case?
	- Are you a weak person? In what way? Why is that?

---

# What can I do ? :sob:
##### Should I take drugs :pill: ?
<!-- NOTE: No!.... Well yes! -->

---

#  There's two ways :sunglasses:
![bg opacity:.2](matrix-pills.jpg)
You take the :large_blue_circle: - the story ends, you wake up in your bed and believe whatever you want to believe.
You take the :red_circle: - you stay in Wonderland, and I show you how deep the rabbit hole goes
### What will you choose ?

---

# :red_circle: Let's go full fantasy !
##### ![w:480](gandalf.jpg)

---

### Let's define the team by creating
# RPG characters
# ![w:640](comic.jpg)
<!-- NOTE: -->

---

# Fate Accelerated
#### (based on)

---


<!-- NOTE: How we do that ? -->
# 3 Steps
### Aspects, Prose, Interpretation

---

# 4 aspects
### High concept, Trouble, Personal story, Crossroad
<!--
1. High concept
2. Trouble
3. Personal story
4. Crossroad
-->

---

# High concept
### One sentece that defines the PC

---

# High concept
### One sentece that defines the PC

* The sentence...
  - sums up **who** they are
  - tells what they're doing for the living
  - tells what makes them **different** from others
* Think on...
  - in what they could help you
  - how could them complicate your life

---

# Trouble
### How you use to complicate your life ?

---

# Trouble
### How you use to complicate your life ?

- Personal weakness
- Recurrent enemy
- Important obligation
- ...

---

# Personal story
### How they reacted to an important situation they were involved ?

* Problem :arrow_right: Resolution

---

# Crossroad

### How they reacted to an important situation they were passing by ?


<!-- One person starts a story leaving the resolution open, and other person explains how they PC resolves it. -->

---

# Example

1. High concept: arquitecto romano *políglota*
2. Trouble: *amante de la naturaleza*
3. Personal story: se iba a casar y lo dejaron plantado por alguien más rico, ahora es *precavido* y pasa del dinero
4. Crossroad: se encuentra un conflicto en el mercado, un tendero acusa a un pobre de robarle una manzana y este dice que se la han dado. Que hace? Le paga la manzana al tendero, *humanitario*

---

# *Prose* it
#### Using a (some) sentences using *aspects*

---

# Example

*políglota*, *amante de la naturaleza*, *precavido*, *humanitario*

Es capaz de hablar con todos, cuidadoso al proyectar, si no va a dejar mejor el paisaje no lo hace, su objetivo final no es el dinero, evita los aprovechados, asume algunos pequeños costes si esto mejora el mundo.

---

# :large_blue_circle: Return back to reality
#### Re-interpret the definition as it was defining the team

---

# Example
- Está cerca del usuario, habla su idioma y entiende sus problemas.
- Sólo trabaja donde hay una oportunidad clara de mejora.
- Aportación a la ciencia, no sólo porque aporte dinero.
- No le hace el trabajo a los demás, sino que los ayuda, especialmente si con poco esfuerzo ayuda mucho.

---

# Build a party
#### Use items to define the team
#### (aka Join :arrow_right: Discuss :arrow_right: Select)

---

<!--
paginate: false
_footer: ' '
-->
# <!-- fit --> Hope you enjoyed it ! :grin:
![bg opacity 70% right](badge.svg)
![w:60px](adsmurai.png) Adsmurai

Passeig de Gràcia, 60 4º
08007 Barcelona
Phone: +34 931 222 301
Spain

📧 research@adsmurai.com
