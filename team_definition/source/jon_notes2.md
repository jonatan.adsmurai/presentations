# Like a Fate's character

- Concepto y complicación
- Aspecto
- Nombre y apariencia
- Estilos
- +2 aspectos
- Proeza


CONCEPTO PRINCIPAL
En primer lugar, decide el concepto principal de tu personaje. Se trata de una
sola frase u oración que resume claramente quién es tu personaje, a qué se dedica
y qué es lo que le diferencia de los demás. Cuando pienses en tu concepto princi-
pal, intenta decidir dos cosas: en qué puede ayudarte y cómo puede complicarte
la vida. Un buen concepto principal logra ambas cosas.

COMPLICACIÓN
A continuación, decide cómo te sueles complicar la vida. Podría tratarse de una
debilidad personal, un enemigo recurrente o una obligación importante; cual-
quier cosa que te complique la vida.

OTRO ASPECTO
Ahora escribe otro aspecto. Piensa en algo realmente importante o interesante
acerca de tu personaje. ¿Es la persona más fuerte de su pueblo? ¿Lleva consigo una
poderosa espada cuya fama es conocida desde hace siglos? ¿Habla por los codos?
¿Le sobra el dinero?

OPCIONALMENTE: UNO O DOS ASPECTOS MÁS
Si lo deseas, puedes crear uno o dos aspectos más. Dichos aspectos pueden des-
cribir la relación de tu personaje con el personaje de otro jugador o con un PNJ.
O bien, al igual que el tercer aspecto que escribiste anteriormente, podría añadir
algo especialmente interesante acerca de tu personaje.
Si lo prefieres, puedes dejar uno de esos aspectos (o los dos) en blanco y añadir-
los más tarde, cuando hayas comenzado a jugar.

NOMBRE Y APARIENCIA
Describe la apariencia del personaje y dale un nombre.

ESTILOS
Cauto
Furtivo
Ingenioso
Llamativo
Rápido
Vigoroso

Cauto: Una acción es Cauta cuando prestas mucha atención a los detalles y
te tomas tu tiempo para hacerla bien, como apuntar con sumo cuidado una
HACER COSAS: RESULTADOS, ESTILOS Y ACCIONES
17flecha para acertar a un blanco lejano, vigilar algo atentamente o desactivar
el sistema de alarma de un banco.
• Ingenioso: Una acción Ingeniosa requiere pensar rápido, solucionar proble-
mas o tener en cuenta variables complejas, como descubrir un fallo en el
estilo de un espadachín rival, encontrar un punto débil en el muro de una
fortaleza o arreglar un ordenador.
• Furtivo: Una acción Furtiva tiene como fin despistar, pasar inadvertido o
engañar a alguien, como convencer a la policía para que no te detenga, robar
una cartera o hacer una finta durante una lucha con espadas.
• Llamativo: Una acción Llamativa llama la atención sobre ti; es una exhibi-
ción de estilo y audacia, como pronunciar una arenga que levante la moral
de tu ejército, avergonzar a tu contrincante en un duelo o crear un castillo de
fuegos artificiales mágicos.
• Rápido: Una acción Rápida supone moverse de forma veloz y ágil, como es-
quivar una flecha, lanzar el primer puñetazo o desactivar una bomba cuando
apenas quedan unos segundos para que estalle (tic-tac-tic-tac-tic-tac...).
• Vigoroso: Una acción Vigorosa es una demostración de fuerza bruta nada
sutil, como forcejear con un oso, intimidar a un matón con la mirada o
lanzar un hechizo mágico muy potente.


Un aspecto es una palabra o frase que describe algo especial acerca de una per-
sona, como un lugar, una cosa, una situación o un grupo. Casi todo lo que se te
ocurra puede tener aspectos
