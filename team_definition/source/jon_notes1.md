Com si fos un PJ, un súper heroi

Nom, Característiques/valors, habilitats,...
Imatge: MLAAR indi

Habilidades:
Aspectos: tu pj ES cosas

(accel) Approach/Estilos: Cauto (calma, pensando), Furtivo (que no te vean), Ingenioso (pensamiento rápido), Llamativo (atraes atención), Rápido (sin pensar, físico), Vigoroso (por la fuerza bruta, a las bravas)


## MLAAR

Ice breaker: pensar en un personaje de película, libro, teatro, dibujos, ... Que os mole y explicar algún detalle o porque os gusta.

### Idea 1
Concepto: arquitecto romano *poliglota*
Complicación: *amante de la naturaleza*
Historia inicial: se iba a casar y lo dejaron plantado por alguien más rico, ahora es *precavido* y pasa del dinero
Cruce de caminos: se encuentra un conflicto en el mercado, un tendero acusa a un pobre de robarle una manzana y este dice que se la han dado. Que hace? Le paga la manzana al tendero, *humanitario*

Desarrollo ?
Capaz de hablar con todos, cuidadoso al proyectar, si no va a dejar mejor el paisaje no lo hace, su objetivo final no es el dinero, evita gorrones / aprovechados, asume algunos pequeños costes si esto mejora el mundo.

Mundo real?
- Cerca del usuario, habla su idioma, entiende sus problemas.
- - Sólo trabaja donde hay una oportunidad clara de mejora.
- Aportación a la ciencia, no sólo a la pasta
- - No le hace el trabajo a los demás, sino que los ayuda, especialmente si con poco esfuerzo ayuda mucho.


### Idea 2
Concepto: professor de historia de la República *gran comunicador*
Complicación: *comprometido con sus ideales*
Historia inicial: su abuelo murió en el campo a manos de un cacique, ahora *huye de los poderosos*
Cruce de caminos: una esposa quiere irse de casa por maltrato psicológico, pero esta prohibido, se la encuentra huyendo por el bosque, con su marido a lo lejos buscándola. Que haces? Esconde a la chica en su casa, *tiene su propia justicia*

Desarrollo?
- explica las cosas para que todos la entiendan
- - se guia por su corazón, emocional
- no le gusta que estén por encima suyo
- - personas por delante la ley / poder

Mundo real?
- visibilidad a todo lo que hace
- - no siempre hace las cosas por la razón y de forma premeditada, si algo le motiva lo acciona
- no a que le manden que hacer, auto-gestiona
- - personas delante procesos, lo que ayuda a la gente antes que lo que hace solo dinero
