---
title: Zero-Shot Fashion Products Clustering on Social Image Streams
theme: default
class:
  - invert
  - lead
size: 16:9
paginate: true
footer: Query-by-image Fashion Product Retrieval on Social Image Streams
author: Jonatan Poveda
---

<!--
_paginate: false
_header: ![height:96](logoUPC.png) ![height:72](adsmurai.svg)
_footer: 'This work is partially supported by the Spanish Ministry of Economy and Competitivity under contract TIN2015-65316-P and by the SGR programme (2014-SGR-1051 and 2017-SGR-962) of the Catalan Government.'
-->

<!-- Query-by-image Fashion Product Retrieval on Social Image Streams -->
# Zero-Shot Fashion Products Clustering on Social Image Streams
## Jonatan Poveda - LOD2019

---

<!--
_header: MOTIVATION ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

# Motivation & Proposal

---

<!-- 
_header: MOTIVATION ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

# Motivation
##### Image-to-Product Matching: we want to relate street images with product images
##### Is a problem difficult to solve directly, therefore we try to **ease it** first
<!-- ![](product-retrieve.png) -->
<p align="center"><img src="product-retrieve.png"></p>

---

<!-- 
_header: MOTIVATION ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

# Proposal 
#### **Goal**: increase relevance of images to curate from
#### **Proposal**: find a subset of product candidates and rank them by *similarity* 
#### *Similarity* is defined as ‘same product type’ (metadata)

---

<!-- 
_header: MOTIVATION ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

# Characteristics

- Available metadata could be unreliable in general
  * **Use images**
- Just one or very few (1-4) catalogue images per product available
  * **Learn from few samples**
- Catalogue is updated every day: few addons, deletions, and updates
- Catalogue is seasonal: massive changes
  * **Robust to changes**

---

<!-- 
_header: MOTIVATION ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

# Characteristics of images and products

- Bad light conditions
- Multiple items may appear on the scene
- Occlusions
- Non-rigid transformations
- Different products that look the same
- There is not a common taxonomy

--- 

<!-- 
_header: Motivation ▶ PROBLEM ▶ Solution ▶ Optimization ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

# Problem
## Zero-shot Learning

---

<!--
# Zero-shot Learning

Problem: Learn how to describe labeled data, so unlabeled can be described using the same vocabulary.

![Slide 13 image](zsl.png)

Train: subsample from some classes
Predict: some samples of unseen classes

![ian goodfellow](ian.png)
![tom musgrove](tom.png)

---
-->

<!-- 
_header: Motivation ▶ PROBLEM ▶ Solution ▶ Optimization ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

# Zero-shot Learning
![Slide 13 image](zsl.png)
###

---

<!-- 
_header: Motivation ▶ PROBLEM ▶ Solution ▶ Optimization ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

# Zero-shot Learning
![ian goodfellow](ian.png)
###

---

<!-- 
_header: Motivation ▶ Problem ▶ SOLUTION ▶ Optimization ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

# A solution
## Distance Metric Learning (DML) 

---

<!-- 
_header: Motivation ▶ Problem ▶ SOLUTION ▶ Optimization ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

## Distance Metric Learning (DML) 
![slide 15-16 image](dml.png)

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ OPTIMIZATION ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

# An optimization
## Proxy-NCA

#
#
###### Movshovitz-Attias, Yair, et al. "No fuss distance metric learning using proxies." Proceedings of the IEEE International Conference on Computer Vision. 2017. https://arxiv.org/pdf/1703.07464.pdf
###### Github implementation: https://github.com/dichotomies/proxy-nca

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ OPTIMIZATION ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

## Proxy-NCA
To reduce computational cost, 
Build all possible triplets $O(n^3)$ → Build triplets with approx. of samples (proxies)

<!-- ![height:300](pnca1.png) -->
<p align="center"><img height=300 src="pnca1.png"></p>

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ OPTIMIZATION ▶ Method ▶ Experiments ▶ Results ▶ Conclusions
-->

## Proxy-NCA
![slide 32](pnca2.png)

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ METHOD ▶ Experiments ▶ Results ▶ Conclusions
-->

# Method
## Dataset: Modanet
<!-- ![height:300](modanet.png) -->
<img align="left" height="360" src="modanet.png"> Fashion garments
55k images 
265k items
13 meta-categories
\
We use a subset: 3,376 samples per class.
Labels: {*similar*, *dissimilar*}

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ METHOD ▶ Experiments ▶ Results ▶ Conclusions
-->

## Architecture

- BN-Inception: pre-trained on Imagenet2012
- Image size: 3x256x256
- Batch size: 32
- Embedding size: 64
- Triplet loss
- Adam optimizer
- Implemented in PyTorch

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ METHOD ▶ Experiments ▶ Results ▶ Conclusions
-->

## Architecture: training
![slide 36](arch-train.png)

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ METHOD ▶ Experiments ▶ Results ▶ Conclusions
-->

## Architecture: evaluation
![slide 37](arch-eval.png)

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ METHOD ▶ Experiments ▶ Results ▶ Conclusions
-->

## Metrics

![](metrics.png)

$~~~~~~~~~~~~~~~~~ NMI = 2 \frac{I(X;Y)}{H(X)+H(Y)}$

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ EXPERIMENTS ▶ Results ▶ Conclusions
-->

# Experiments

**Segmentation - Input normalization**
Weight initialization
Learning rate
Weight updating
**Model generalization**
Performance per class
ZSL vs Classification

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ EXPERIMENTS ▶ Results ▶ Conclusions
-->

## Experiments: segmentation

- Does having a segmentation helps ? 

<!--
- It looks segmentation does not help on image retrieval
- -Underperforms on clustering
-->

![height:360](exp1.png)

Comparison may not be fair due to input normalization

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ EXPERIMENTS ▶ Results ▶ Conclusions
-->

## Experiments: input normalization

Comparison may not be fair due to input normalization

![slide 42](norm.png)

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ EXPERIMENTS ▶ Results ▶ Conclusions
-->

## Experiments: mask-aware input normalization

Does a mask-aware input normalization boost performance on segmentation ? 

![height:360](exp2.png)

<!--
Similar behaviour on retrieval metrics
Better performance on NMI
Seems using segmentation does not worth it ⇒ from here onwards we only use bounding box
-->

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ EXPERIMENTS ▶ Results ▶ Conclusions
-->

## Experiment: model generalization

How classes in training are affecting to performance on validation ?

![](general1.png)

---

<!--
# Experiment: model generalization

Is learning *dress* and *top* helping on *outer* ?
Is *belt* and *skirt* helping on *shorts* ?
Is *boots* and *pants* helping on *footwear*?

---
-->

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ EXPERIMENTS ▶ Results ▶ Conclusions
-->

## Experiment: model generalization

Picking the right classes clearly improves both retrieval and clustering performance

![](general2.png)

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ Experiments ▶ RESULTS ▶ Conclusions
-->

# Results

![](results.png)

---

<!-- 
_header: Motivation ▶ Problem ▶ Solution ▶ Optimization ▶ Method ▶ Experiments ▶ Results ▶ CONCLUSIONS
-->

# Conclusions

- Retrieval: difficult but feasible (R@1 = 0.78)
- Proxies alleviates outstandingly the training load
- On few #classes, picking the right classes is key
- Using segmentation does not seem to worth it
- Be aware of the nature of our data
- Extra effort on small and deformable objects

---

# WIP
  
- Is clustering by class is helping to place closer visually similar (like colour) samples?
- How does it perform on larger number of classes?
- What could we do to increase performance on small objects?

---

<!--
_footer: ''
-->

# Thank you for your attention. Q ? 

