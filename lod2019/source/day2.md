---
---

# few-shot

leverage knowledge of diff classes seen before
transfer: features, how to adapt to new tasks (metalearn)

old: multitask: p(y|x, global params, task specific param)=theta, train specif params D(x,y) = phi)
pred : (y*|x*, theta, estim phi) .. 2-step


new: p(y*|x*, global p, phi(D)), phi is a net
1-step, directly specify the predictive distribution ~ 
(conditional neural processs + auto-regressive models)


train by max likelihood

Adapting a class: diff head for each task
adap the feature extractor: FiLM layers (feature-wise linear modulation) - scales and shifts features that are useful for an specf task
- expressive feature adapt, small num of new params

woks; pointnet, deepsets, statistics network, continuarl learning supported by running averages (input: multiple images ~ invariant to the order) - continuous learning is easy, just sum!
conditional neural processs + auto-regressive models)) 

C-NAPs
 
1. pre-train feat extract and linear class
1. train feature extractor adapt network for multiple im
1. pass caps to head adapt network
1. linear classs
1. classify

better on active learning

sum

leverage multiple related ds (multi-task / meta-learn)

very data eff (suport few-shot lear
naturall supp continual learn
robust: handle test conditioons which differ from train cond
has well-calibrated uncertainties (eg. supp active learng)
perform ... very quick
can extend ...

fast!! learning new tasks at test time 

---

# Combinatorial Learning in Traffic Management, Giorgio Sartor

pool of constrains that adapts on time, easy to interpret, combinatorial

---


# Ami Tavory: “Unsupervised Estimation of Principal Component Dimensions through the Principle of Minimum Distance Length”

MDL principle

¿?

---

# Erik Berglund: “Modelling chaotic time series using recursive deep self-organising neural networks”

deterministic, nonlinear, highly sensitive to init cond, bounded

mackley-glass eq (1 var chaos system)

self-org maps (som) - unsupervised, clustering, dim red
plsom (param-less som)

y = invFourier(Fourier(x))   x: chaotic, y: not chaotic

+layers the better
recursive plsom2 can learn to mimic chaos

---

# Robin Vogel, “On Tree-based Methods for Similarity Learning”

TreeRank , general approach to ranking prob
sim learning is eval by ranking criterions, can be adapted for sim learning, U-statistics, depends onexpressivety of C

formulations, math demostrations, .... :cucumber: 

---

# Stefano De Blasi: “Active Learning Approach for Safe Process Parameter Tuning

long-term opt alg for changing systems

---

# Federated Learning of Deep Neural Decision Forests, Anders Sjöberg

FL utilizes ML without storing data centrally

1. server serve a model to clients,
1. clients update their model with local dataset, 
1. they return the model to the server,
1. repeat until criterion reached

client send the gradient

RandomForest: versatile, efficient, divide-conquer, not suited to FL (not differencialbe, no clear way to further train it

but, DNDF can be trained by SGD

Regular DT -> Stochastic DT -> (params) Neural Network

DN + FC + Tree (avg of the forest)

The more trees the faster (less comunications) the convergence

OODIDA framework

---

# Conditional Anomaly Detection for ...  , Eva Jabbar

PhD

---

# Data Anonymization Aware ML, David Jaidan

SCALIAN

text - data enc - data anonym - utility & privacy tradeoff
(imdb - tf-idf / skip-through - dpca / ppca / ..  )

timeseries: data - fft - anonym - anomaly detec model (AUC)

XGBoost outperm

more anonym the less the acc


---

# Thomas Jatschka: “Exploiting Similar Behavior of Users in a Cooperative Optimization Approach for Distributing Service Points in Mobility Applications”

service point distribution problem: place services (like bikes, charging stations, parking) on a city

COA cooperative optim approach, EvoCOP 2019

aggregate user cases by similar preference of users
reduce user fb: matrix factorization of ratings-users

mixed integer programming ¿?

