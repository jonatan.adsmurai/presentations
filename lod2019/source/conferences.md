---
title: Query-by-image Fashion Product Retrieval on Social Image Streams
theme: default
class:
  - invert
  - lead
size: 16:9
paginate: true
footer: Query-by-image Fashion Product Retrieval on Social Image Streams
author: Jonatan Poveda

---

<!--
_paginate: false
_footer: 'Query-by-image Fashion Product Retrieval on Social Image Streams'

-->

---

<!--_class: lead-->
# Deep Neural Network Ensembles, Sean Tao

---

# Deep Neural Network Ensembles, Sean Tao

Each hidden node split the output space

Cluster path: clusters exist in hidden layers

----

# LIA: A Label-Independent Algorithm for Feature Selection for Supervised Learning, Gail Gilboa Freedman

challenge: seleect subset of featuers on pre-proc when data is unlabeled
unsuper comparable to superv to feature selection? yes
label: normal, abnormal (just to eval)
Louvain (fast unfolding of comm...)
network-based partition of the data -> visualize data in another way
reduces cost of processing data, collecting, ... as we perform before labeling
robust: no need to asusme linear relations bet feates
performance: all features < LIA > MRMR
k strongly depens on the dataset
to init: select most similar sample of each community

> it tel ls you when you have too much data (deps on divertisty, ...)

---

# A New Baseline for Automated Hyper-Parameter Optimization, Marius Geitle

Black box vs. Knowledge-based

Methods:
- Simple:   grid s $\less than$ random s  
- Sophisticated: bayesian opt
- Evolutionary: L-SHADE

Next step further from RandomSearch ? **Adaptative Random**

It is simple, comparable to LSHADE (best), trivially parallelizable !

---

# Stochastic Weight Matrix-based Regularization Methods for Deep Neural Networks, Patrik Reizinger

To fight overfitting
Classic: l1 or l2-norms, dropout, batch norm, additive white noise
Weight re-init, weight shuffling (locally)

hyperp -> sampling -> nn + WMM -> loss -> optim looop

unkown datasets: jsb chorales

- good for noisy data
- entropy reduction: good for net compression
- arch independent
- at least same results as others

Links: [repo](http://github.com/rpatrik96/lod-wmm-2019)

---

# Quantitative and Ontology-Based Comparison of Explanations for Image Classification, Alan Perotti

Dangerous not being able to explain: biased, sexists, .. right for wrong reasons, adversarial 
vulnerability, no trust from experts, GDPR non-compilancy (service denied for some reason: black 
people?)

DARPA XAI, LIME, RISE

"this is a cat because I say so" vs "this is a cat: it has fur, whiskers and claws, and this 
feature

local explanations

model-based / model-aware vs model-agnostic

metric: fidelity and interpretability (maching blackb vs match human percept) -> XOR

LIME is cool

you don't want bg (context) affects on your classification (a ladybug on a beach is a ladybug)

get label from Image net, go up&down ontology to grasp the explanation of all of them

obtain counterfactual expl: why is a cat and not a tiger? 

1vsAll penalizes equally similar than completely diff classes (softmax), thats not fair

ontoogical distance (semantic distance)

> methods do not see as we and that is tremendly dangerous

heatmap is not sufficient

promising: Deep Explanations explloting Wordnet (part-links, ...)
ideal black b should be trained with explanations in mind

---

# About generative aspects of Variatonal Autoencoders, Andrea Asperti

Can we generate data by sampling in the latent space ? no... we dont know their distribution
VAE try to force latent vars to have a kwno dist (eg normal dist)

recent research on VAEs: problem is due to difformity betweeen latent dist and the normal prior

contrib: expect the kl divergence will force two firsts moments to agree 
with thos of normal dist (hardly presume the same for other moments)

---

# Rethinking.. Reinforcement Learning

reactive behaviour: trial-error to train -> we need to undersand and reason about world
classically solved by planning (search for the best action): expensive, indirect, ..

planning <--> learning

monte-carlo tree searcAh MCTS (MCTSNet)

Imagination Augmented Agents (I2A)

Items to eval: generalization, data efficiency (solve some or all), time scalability (more time is better? (without more data))

Deep Recurrent Conv-LSTM (DRC) wins RNN, ...

[sokoban](http://sites.google.com/view/modelfreeplaning)

---

# BowTie - a deep learning feedforward neural network for sentiment analysis, Apostol Vassilev

(NLP)

Cybersecurity
secrecy of keys and internal state
ideal :ok_hand: -> in practise implementation is not flawless  (implem vulnerabilities)
attacks exploits diff beween ideal and real implementation
dual thinking of humans: fast and slow -> we believe 'slow' is always there but actually the hero is the 'fast' one: intuition
'fast' is prone to errors but we cannot turn it off

In NLP is not obvious what to do

how much content ?
numerical errors over iterations (poor def matrices, etc..)

Feedforward neural net `||--··--||` looks like of autoencoder 

Limitation of the methodoly or ?

Polarity-weighted multihot encoding text

Datasets: SLMRD, KID

how can i trust a system that is not 100% accurate? (is to evaluate other system)

---

# To What Extent Can Text Classification Help with Making Inferences About Students' Understanding, Twba Al-Shaghdari

(NLP)

Resarch q: can ml help in automatic class of learning journals to predict whether students need support in their learning ?

Acc is not for unbalanced

---

# Building Iride: how to mix deep learnding and Ontologies techniques to understand language, Raniero Romagnoli and Vincenzo Sciacca

> San jose cops kill man with knife. who has the knife?

turn info into knowledge, speech2text, dialogue..

gof-ai, ml , dl

iride: nlp almawave
is not plug&play, some DS involved 
language-dependant
ES-Lucene pipeline

suggestion of class to avoid multiple combo boxes

use: word embed + lang modeling, text class (convol), seq labeling (RNNs, lstm)

arxiv 1907.07526
 
"hiddes" telling user for mispellings (compute dist to other words in bg) 



---


